package com.example.mrhead

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView


class MainActivity : AppCompatActivity() {

    // Deklarasi Variabel
    private lateinit var Rambut: ImageView
    private lateinit var Alis: ImageView
    private lateinit var Janggut: ImageView
    private lateinit var Kumis: ImageView
    private lateinit var cekBoxRambut: CheckBox
    private lateinit var cekBoxAlis: CheckBox
    private lateinit var cekBoxJanggut: CheckBox
    private lateinit var cekBoxKumis: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Inisialisasi Variabel
        Rambut = findViewById(R.id.Rambut)
        Alis = findViewById(R.id.Alis)
        Kumis = findViewById(R.id.Kumis)
        Janggut = findViewById(R.id.Janggut)
        cekBoxRambut = findViewById(R.id.cRambut)
        cekBoxAlis = findViewById(R.id.cAlis)
        cekBoxJanggut = findViewById(R.id.cJanggut)
        cekBoxKumis = findViewById(R.id. cKumis) // inisialisasi CekBoxKumis

        // Metode Cek Rambut
        cekBoxRambut.setOnClickListener {
            Rambut.visibility = if (cekBoxRambut.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Alis
        cekBoxAlis.setOnClickListener {
            Alis.visibility = if (cekBoxAlis.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Kumis
        cekBoxKumis.setOnClickListener {
            Kumis.visibility = if (cekBoxKumis.isChecked) View.VISIBLE else View.GONE
        }

        // Metode Cek Jenggot
        cekBoxJanggut.setOnClickListener {
            Janggut.visibility = if (cekBoxJanggut.isChecked) View.VISIBLE else View.GONE
        }
    }
}